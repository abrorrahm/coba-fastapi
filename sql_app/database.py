from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from urllib.parse import quote_plus as urlquote

SQLALCHEMY_DATABASE_URL = "mysql+mysqlconnector://root:%s@192.168.1.12/db_app" %urlquote('Kangguru@123')
# engine = create_engine('postgres://user:%s@host/database' % urlquote('badpass'))
engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

