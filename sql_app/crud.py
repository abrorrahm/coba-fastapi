from http.client import HTTPException
import imp
from turtle import up
from typing import List
from unicodedata import name
import uuid
from sqlmodel import Session
from sqlalchemy.orm import query
from sqlalchemy.sql import text, column
from sqlalchemy import create_engine, MetaData, Table, Column, Numeric, Integer, VARCHAR, update, subquery
import bcrypt
from . import models, schemas
import hashlib
from devtools import debug


# def get_user_by_username(db: Session, username: str):
#     return db.query(models.UserInfo).filter(models.UserInfo.username == username).first()


def get_user_by_username(db: Session, username: str):
    db_user = db.query(models.UserInfo).get(username)
    if db_user is None:
        return db.query(models.UserInfo).filter(models.UserInfo.username == username).first()
    return db_user


def get_all_users(session: Session, limit: int, offset: int):
    return session.query(models.UserInfo).offset(offset).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    # salt = uuid.uuid4().hex
    hashed_password = hashlib.md5(user.password.encode('utf-8')).hexdigest()
    debug(hashlib.md5(user.password.encode('utf-8')))
    debug(hashed_password)
    db_user = models.UserInfo(username=user.username, password=hashed_password, nama=user.nama)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_user_info(db: Session, username: str, user:schemas.UserUpdate):
    db_user = get_user_by_username(db, username)
    
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    
    db_user.password = hashlib.md5(user.password.encode('utf-8')).hexdigest()
    db_user.nama = user.nama
    
    db.commit()
    db.refresh(db_user)
    
    return db_user
    

def check_username_password(db: Session, user: schemas.UserAuthenticate):
    db_user_info: models.UserInfo = get_user_by_username(db, username=user.username)
    return hashlib.md5(user.password.encode('utf-8') + db_user_info.password.encode('utf-8'))


def delete_user_info(db: Session, username: int):
    user_info = get_user_by_username(db, username)

    if user_info is None:
        raise HTTPException(status_code=404, detail="User not found")

    db.delete(user_info)
    db.commit()

    return "Username berhasil dihapus"


def create_new_blog(db: Session, blog: schemas.BlogBase):
    db_blog = models.Blog(title=blog.title, content=blog.content)
    db.add(db_blog)
    db.commit()
    db.refresh(db_blog)
    return db_blog


def get_all_blogs(db: Session):
    return db.query(models.Blog).all()


def get_blog_by_id(db: Session, blog_id: int):
    return db.query(models.Blog).filter(models.Blog.id == blog_id).first()



def delete_blog_by_id(db:Session, blog: schemas.Blog):
    db.delete(blog)
    db.commit()