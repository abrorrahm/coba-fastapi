from typing import List
from unicodedata import name
from pydantic import BaseModel
from sqlmodel import Field, Session, SQLModel, create_engine, select


class UserInfoBase(BaseModel):
    username: str


class UserCreate(UserInfoBase):
    nama: str
    password: str
    

class UserUpdate(BaseModel):
    nama: str
    password: str


class UserAuthenticate(UserInfoBase):
    password: str


class UserInfo(UserInfoBase):
    username: str

    class Config:
        orm_mode = True


class PaginatedUserInfo(BaseModel):
    limit: int
    offset: int
    data: List[UserInfo]


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str = None


class BlogBase(BaseModel):
    title: str
    content: str


class Blog(BlogBase):
    id: int

    class Config:
        orm_mode = True