Install pip:
- Download the script, from https://bootstrap.pypa.io/get-pip.py.
- Open a terminal/command prompt, cd to the folder containing the get-pip.py file and run:
    python get-pip.py

Virtual env:
- pip install virtualenv
- virtualenv env
- mypthon\Scripts\activate

Requirements:
- pip install requirements.txt

Run:
- uvicorn main:app --reload  
